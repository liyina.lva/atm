# -*- coding: utf-8 -*-
from money_handler import MoneyHandler
from money_enum import MoneyEnum


class TwentyMoneyHandler(MoneyHandler):

    def handle_request(self, money_required):
        # if request is MoneyEnum.TWENTY:
        if money_required >= MoneyEnum.TWENTY.value:
            number_of_notes = money_required / MoneyEnum.TWENTY.value
            remaining_amount = money_required % MoneyEnum.TWENTY.value
            print("TwentyMoneyHandler handles " + MoneyEnum.TWENTY.name)
            print("TwentyMoneyHandler handles " + str(number_of_notes))
            if number_of_notes != 0:
                self._next_handler.handle_request(remaining_amount)
        else:
            print("TwentyMoneyHandler doesn't handle " + MoneyEnum.TWENTY.name)
            if self._next_handler is not None:
                self._next_handler.handle_request(money_required)
