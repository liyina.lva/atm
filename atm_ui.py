# -*- coding: utf-8 -*-
from random import randrange

from hundred_money_handler import HundredMoneyHandler
from fifty_money_handler import FiftyMoneyHandler
from twenty_monery_handler import TwentyMoneyHandler
from ten_money_handler import TenMoneyHandler
from money_enum import MoneyEnum


def _get_number_of_notes():
    return randrange(20)


def set_up_chain():
    hundred_money = HundredMoneyHandler(number_of_notes=_get_number_of_notes)
    fifty_money = FiftyMoneyHandler(number_of_notes=_get_number_of_notes)
    twenty_money = TwentyMoneyHandler(number_of_notes=_get_number_of_notes)
    ten_money = TenMoneyHandler(number_of_notes=_get_number_of_notes)

    hundred_money.set_next_handler(fifty_money)
    fifty_money.set_next_handler(twenty_money)
    twenty_money.set_next_handler(ten_money)

    return hundred_money


if __name__ == '__main__':
    chain = set_up_chain()

    money_required = 250

    chain.handle_request(money_required)
