# -*- coding: utf-8 -*-
import abc


class MoneyHandler(metaclass=abc.ABCMeta):

    def __init__(self, successor=None, number_of_notes=None):
        self._next_handler = successor
        self._number_of_notes = number_of_notes

    @abc.abstractmethod
    def handle_request(self, money_required):
        pass

    def set_next_handler(self, handler):
        self._next_handler = handler
