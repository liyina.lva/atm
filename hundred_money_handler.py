# -*- coding: utf-8 -*-
from money_handler import MoneyHandler
from money_enum import MoneyEnum


class HundredMoneyHandler(MoneyHandler):

    def handle_request(self, money_required):
        if money_required >= MoneyEnum.HUNDRED.value:
            number_of_notes = money_required / MoneyEnum.HUNDRED.value
            remaining_amount = money_required % MoneyEnum.HUNDRED.value
            print("HundredMoneyHandler handles " + MoneyEnum.HUNDRED.name)
            print("HundredMoneyHandler handles " + str(number_of_notes))
            if number_of_notes != 0:
                self._next_handler.handle_request(remaining_amount)
        else:
            print("HundredMoneyHandler doesn't handle " + MoneyEnum.HUNDRED.name)
            if self._next_handler is not None:
                self._next_handler.handle_request(money_required)
