# -*- coding: utf-8 -*-
from money_handler import MoneyHandler
from money_enum import MoneyEnum


class FiftyMoneyHandler(MoneyHandler):

    def handle_request(self, money_required):
        # if request is MoneyEnum.FIFTY:
        if money_required >= MoneyEnum.FIFTY.value:
            number_of_notes = money_required / MoneyEnum.FIFTY.value
            remaining_amount = money_required % MoneyEnum.FIFTY.value
            print("FiftyMoneyHandler handles " + MoneyEnum.FIFTY.name)
            print("FiftyMoneyHandler handles " + str(number_of_notes))
            if number_of_notes != 0:
                self._next_handler.handle_request(remaining_amount)
        else:
            print("FiftyMoneyHandler doesn't handle " + MoneyEnum.FIFTY.name)
            if self._next_handler is not None:
                self._next_handler.handle_request( money_required)
