# -*- coding: utf-8 -*-
from money_handler import MoneyHandler
from money_enum import MoneyEnum


class TenMoneyHandler(MoneyHandler):

    def handle_request(self, money_required):
        # if request is MoneyEnum.TEN:
        #     print("TenMoneyHandler handles " + request.name)

        if money_required >= MoneyEnum.TEN.value:
            number_of_notes = money_required / MoneyEnum.TEN.value
            remaining_amount = money_required % MoneyEnum.TEN.value
            print("TenMoneyHandler handles " + MoneyEnum.TEN.name)
            print("TenMoneyHandler handles " + str(number_of_notes))
            if number_of_notes != 0:
                self._next_handler.handle_request(remaining_amount)
        else:
            print("TenMoneyHandler doesn't handle " + MoneyEnum.TEN.name)
            if self._next_handler is not None:
                self._next_handler.handle_request(money_required)
