# -*- coding: utf-8 -*-
from enum import IntEnum


class MoneyEnum(IntEnum):
    HUNDRED = 100
    FIFTY = 50
    TWENTY = 20
    TEN = 10
