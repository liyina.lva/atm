# -*- coding: utf-8 -*-


class SubjectAtm(object):

    def __init__(self):
        self.money = 0

    def add(self, money_handler):
        if money_handler not in self.money_handlers:
            self.money_handlers.append(money_handler)

    def get_money_handler(self, *args, **kwargs):
        for money_handler in self.money_handlers:
            money_handler.active(*args, **kwargs)
